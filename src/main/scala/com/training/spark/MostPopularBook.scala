package com.training.spark

import org.apache.spark.sql.SparkSession

object MostPopularBook {

  def main(args: Array[String]): Unit = {

    val input: String = args(0)
    val output: String = args(1)

    val spark = SparkSession
      .builder()
      .appName("Most popular book")
      .master("local[*]")
      .getOrCreate()

    val rows = spark.sparkContext.textFile(input)
      .filter(line => !line.contains("User-ID"))
      .map(line => line.substring(1, line.length - 1))
      .map(line => line.split("\";\""))
      .map(items => (items(1), items(2).toInt))


    val grouped = rows.groupByKey()
      .map(row => (row._1, row._2.size))
      .sortBy(row => row._2, false)

    grouped.saveAsTextFile(output)

    spark.stop()
  }

}
